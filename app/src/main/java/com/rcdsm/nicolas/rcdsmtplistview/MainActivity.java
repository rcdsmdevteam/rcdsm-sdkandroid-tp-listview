package com.rcdsm.nicolas.rcdsmtplistview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;


public class MainActivity extends Activity {

	protected ListView lv;
	protected ArrayList<Personne> data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//Récupération du ListView du fichier xml
		lv = (ListView) findViewById(R.id.listView);

		//On initialise des données à afficher
		data = new ArrayList<Personne>();
		data.add(new Personne("Meyer", "Adrien", 23));
		data.add(new Personne("Dupond", "Pierre", 82));
		data.add(new Personne("Durant", "Charles", 45));
		data.add(new Personne("Schmol", "Toto", 32));

		// On créé l'arrayAdapter qui va géré le tableau en argument de la manière donnée dans le second argument
		PersonneAdapter a = new PersonneAdapter(this, data);

		lv.setAdapter(a);
	}
}