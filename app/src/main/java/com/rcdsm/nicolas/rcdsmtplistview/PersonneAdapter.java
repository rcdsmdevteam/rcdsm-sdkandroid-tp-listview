package com.rcdsm.nicolas.rcdsmtplistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nicolas on 12/8/2014.
 */
public class PersonneAdapter extends BaseAdapter{

	protected Context context;
	protected ArrayList<Personne> listDePersonne;

	public PersonneAdapter(Context context, ArrayList<Personne> listDePersonne) {
		this.listDePersonne = listDePersonne;
		this.context = context;
	}

	@Override
	public int getCount() {
		return listDePersonne.size();
	}

	@Override
	public Object getItem(int position) {
		return listDePersonne.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//On récupère un fichier à inflater
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//On inflate le fichier XML pour créé les composants de la vue
		View view = inflater.inflate(R.layout.tp7_itemview, null);

		//On récupère les composants de la vue
		TextView tvNom = (TextView) view.findViewById(R.id.tvNom);
		TextView tvPrenom = (TextView) view.findViewById(R.id.tvPrenom);
		TextView tvAge = (TextView) view.findViewById(R.id.tvAge);

		//On met à jour les composants précédement récupérés à partir des données de la personne n° position
		Personne p = listDePersonne.get(position);
		tvNom.setText(p.getNom());
		tvPrenom.setText(p.getPrenom());
		tvAge.setText(String.valueOf(p.getAge()));

		return view;
	}
}
