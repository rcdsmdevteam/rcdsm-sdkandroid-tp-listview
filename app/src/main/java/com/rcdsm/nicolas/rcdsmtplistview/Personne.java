package com.rcdsm.nicolas.rcdsmtplistview;

/**
 * Created by Nicolas on 12/8/2014.
 */
public class Personne {

	protected String nom;

	public int getAge() {
		return age;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNom() {
		return nom;
	}

	protected String prenom;
	protected int age;

	public Personne(String nom, String prenom, int age) {
		this.age = age;
		this.nom = nom;
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return nom + " " + prenom + ", " + age + " an(s)";
	}
}
